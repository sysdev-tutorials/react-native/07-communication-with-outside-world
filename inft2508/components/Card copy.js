import React from 'react';
import { Text, View, Image } from 'react-native';

const Card = (props) => {
  return (
    <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10,
          justifyContent: "center", 
          alignItems: "center",
        }}>
        <View style= {{flex: 1, justifyContent: "flex-end"}}>
          <Image
            style= {{width: 20, height: 20}}
            source={props.logo}
          />
        </View>
        <View style= {{flex: 1, justifyContent: "flex-start"}}>
          <Text>{props.displayText}</Text>
        </View>
    </View>
  );
}

export default Card;