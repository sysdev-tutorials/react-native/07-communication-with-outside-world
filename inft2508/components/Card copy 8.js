import React from 'react';
import { 
  Text, 
  View, 
  Image,
  Pressable,
 } from 'react-native';

const Card = (props) => {

  const onCardPress = () => {
   props.showItems(props.displayText)
  }
  
  return (
    <Pressable onPress ={onCardPress}>
      <View style={{ 
            backgroundColor: "#9FE8FF",
            height: 100,
            width: 150,
            borderRadius: 10,
            justifyContent: "center", 
            alignItems: "center",
            margin: 10,
          }}>
          <View style= {{flex: 1, justifyContent: "flex-end"}}>
            <Image
              style= {{width: 40, height: 40}}
              source={props.logo}
            />
          </View>
          <View style= {{flex: 1, justifyContent: "flex-start"}}>
            <Text>{props.displayText}</Text>
          </View>
      </View>
    </Pressable>
  );
}

export default Card;