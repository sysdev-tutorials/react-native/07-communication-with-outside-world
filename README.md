# Communication with outside world​

In today´s lesson we will learn how a mobile app can communicate to the outside world  - for example retrieve or update data residing in a remote server.

Specifically we will focus data retrival and update using REST API.

As in previous lessons, we will continue using and improving our app as we learning new concepts.


## Preparations
If you do a fresh start of a repo, copy inft2508 folder from previous lesson (https://gitlab.com/sysdev-tutorials/react-native/06-multi-page-or-screen-application/-/tree/main/inft2508) to your repo!

Recap: Af this stage, if you run the app with sample data, and be able do go back and forth from 'Home' screen and other screens.

## Application data and REST API
So far we have been using a dummy data. Partly they are hardcoded in the source code itself, for example list of 'Cards' displayed in home screen. Partly, data is taken from a local 'dummydata.json' file.

In a more realistic scenaio, the application data oftern changes over time and the data resides on a remote data (database) server. That means mobile app should be able to retrieve and update data located in a remote server! We will achieve this using concept of 'REST API'

### What is REST API
API stands for 'application programming interface' - meaning that it is an interface to a piece of software and tells methods to use it!

REST is a set of architectural constraints or principles. 

That means, REST API basically offers uniform way of accessing a piece of software, usually via HTTP protocol!

This course (and lesson) assumes that you are somewhat familiar with REST API, if not please familiarize yourself. Some useful links are following.

- https://www.geeksforgeeks.org/rest-api-introduction/​
- https://www.astera.com/type/blog/rest-api-definition/​
- https://restfulapi.net/​

### JavaScript libraries for using REST API in react-native app
There are various alternatives for react native app to work with REST APIs. Some commonly used libraries are​

- Fetch (https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)​
- Axios (https://github.com/axios/axios)

Both of these libraries offer wide range of functionalities. Axios probably has wider backward compatibility while Fetch has built-in suport in react-native and is known being readily available in all modern browsers! Usually both these offer functionalities for common use-cases​! In our app, we will use 'Fetch'​

Note also that there are some known limitations with 'Fetch' https://reactnative.dev/docs/network#known-issues-with-fetch-and-cookie-based-authentication​

### Using 'fetch'?
Fetch has native support in react-native, therefore no additional installation is required​!

In this section we will learn how to make REST requests with 'fetch'!

Basic syntact for making request is following

```
fetch (url, {options})
```

Where, ```url``` is the API server URL, and ```options``` are used whenever more customization is needed for example 
- to specify type of HTTP method like GET, POST, PUT etc
- to include specific header parameters
- to include data in the request body

That means, using ```fetch``` requires that we first need a server URL, which is infact we (or mobile app) need a server to talk to. 

In a real-world scenario this will either be developed and managed by mobile app developer or it will be provided bu other providers providing required functionalities via API.

In our case, for demo/testing purposes, we are going to use a mock server! More in the next section.

### Using 'mock' api server
For mock RESt API server, we will use a library called 'json-server'. Official Github page, https://github.com/typicode/json-server
​
Installation can be done either global to the system/machine or local to a project. In the following we will describe how it can be installed local to a project and then therafter used or started!

- Installation can be done by running this command from the project directory (i.e inside inft2508 folder)

    ```​
    npm install --save-dev json-server​
    ```

    The above command will append 'json-server' as dev dependency for the project which can be verified by inspecting 'package.json' file.


- Then append this ```"mockapi": "json-server -w data/dummydata.json"``` to the scripts section in 'package.json' file​. This will enable us to start the json-server by runing the following command from project directory. Note that we have used 'data/dummydata.json' as data source at the point of starting the server and the data can be updated via API calls!

    ```
    npm run mockapi
    ```

Now that our REST API server should be running and ready to accept requests. You should see the server console like the image below.

<img src="json_server_installl.png">


Note that the verification can be run by running the following curl command! This will return a json data - list of restaurant items!

```
curl http://localhost:3000/restaurants
```

### Exposing local server to the internet
In real-world case, you will deploy your API into a server or you may use an API provided by someone else which is already running on server.

But for the development or quick prototyping purpose, you may need to expose your local server to the internet such that your APIs can be quickly tested! For this purpose, one can use a tool called 'ngrok'. This tool can be downloaded, configured and run locally - ref, ```https://ngrok.com/download```. This tool basically creates a secure tunnel between a service in your local machine and 'ngrok' server such that your local service can be exposed pubicly in the internet via/under 'ngrok' domain.

Typical use-case that we will use is to expose API´s provided by 'json-server' in previous section into the public internet using 'ngrok' tool. The steps are following

- Make sure that your 'json-server' is up and running, for example on ```http://localhost:3000```
- Download/install the 'ngrok' tool in your platform. There are different alternative, checkout ```https://ngrok.com/download```. For example, on Mac, it can be installed using 'brew' like ```brew install ngrok/ngrok/ngrok```
- Configure 'authtoken' for the ngrok! Authtoken can be ontained from the ngrok website - you need to create an user and login for that. When you have token, you can configure like this ```ngrok config add-authtoken <token>```
- Final step if to create a tunnel i.e map a local service to the ngrok server, like this ```ngrok http <serviceport>```. Service port in our case, if for example 3000.


### Preparing and use of data
As shown in the earlier section, we will be using our dummydata.json file as datasource for json-server! 

Since ```unique id``` will be needed for each data record, we will be appending ```id``` field to each record - for example to each restautant item, to each music/song item and so on.

Such data will be read from mobile app, via RESI API library ´fetch´, not directly from file as done on previous lesson!


## Reading data using REST API
For reading data from REST API, we are going to use 'fetch' and http 'GET' method.

Simple code snippet to read list of restaurants is following

```
fetch("http://localhost:3000/restaurants")
```

This will result list of restaurants. In our case, we want to populate list of restaurants into a state variable called 'items'. So, in order to achieve that we are going write a reusable function! Like below

```
    const getFoodItems = () => {
        if (category == "Food") {
            // call api
            fetch("http://localhost:3000/restaurants")
                .then((response) => response.json())
                .then( (response) => {
                    setItems(response);
                })
        }
    }
```

Note that REST API calls are asynchronous in nature - they take time! There are mainly two patterns for working asynchronous operations in JavaScript. One is 'chain' pattern and the other one is 'async/await' pattern. 

In the above code snippet, we have used to called 'chain' pattern using 'then' keywords. In this pattern the result from previous step is chained to next step using 'then' function. Within each 'then' function the result can be consumed, updated or returned to next!

The same functionality as above can be written using 'async/await' pattern as below. Take a note of 'async' and 'await' keywords and their usage. A function need to be declared as 'async' function if it uses 'await' keywords somewhere in its body!

This was short introduction on 'chain' and 'async/await' patterns. Do your own research to learn more about these patterns!

```
    const getFoodItems = async () => {
        if (category == "Food") {
            // call api
            const response = await fetch("http://localhost:3000/restaurants");
            const foodItems = await response.json();
            setItems(foodItems);
        }
    }
```

All right, now we have a function that reads data from a server. But how to use that?

We are going to use another hook provided by react-native, called 'useEffect' hook! More on it in the next section.

### Using 'useEffect' hook
The Effect Hook lets you perform side effects in function components. Side effects are something other than component rendering. The useEffect hook offers similar functionalities as componentDidMount, componentDidUpdate, and componentWillUnmount methods in class based components. Have a look at this nice reading for on useEffect - https://dmitripavlutin.com/react-useeffect-explanation/​ 

Some examples of side effect (i.e use cases for useEffect hook) are following​

- Data fetching​
- Setting up a subscription​
- Manually changing the DOM in React component​
- Setting Timer

​The syntax for using 'useEffect' hook is following

<img align="right" src="use_effect.png" width=500>

```
useEffect(callbackFunction, [dependencies])
```

The 'dependencies' argument controls when to invoke the useEffect callbackFunction, independently from the rendering cycle of the component! 
- No argument: This means that the callback function runs after every rendering
- Empty argument: This means that the callback function runs once after initial rendering
- List ofdependencies: This means that the callback function runs once after initial rendering and then after every rendering if any of the given dependencies change

<br clear="both">

Note that 'useEffect' hooks also offers cleanup functionality which is achieved by returning a function. That means if 'useEffect' callback function returns a function that return function acts as a cleanup function and is invoked in the following circumstances


<img align="right" src="use_effect_cleanup.png" width=200>

- Cleanup function is not invoked after intial rendering
- Invoked before the next side effect callback
- Invoked after unmounting the component (i.e when component tears down) to clean up the latest side-effect

Note also that 'useEffect' hook callback function can not be an async function but you can define and then invoke an async function inside the callback itself​!

<br clear="both">

### Reading REST API inside useEffect hook
Now that we have learned how to make an API call using 'fetch', and we have also learned purpose and usage of 'useEffect' hook. Let´s use them together to read the remote data in a mobile app. Below shows code snippet for 'FoodItemsScreen' component where info about restaurants is read from the server!

Running this piece of code will have same user experience as the end of the previous lesson. The difference is how the data is retrieved by the app under the hood! Note that empty set of dependencies are provided to 'useEffect' hook, which means that the callback function will be invoked only once after initial rendering.

```
// contents of FoodItems.js

import React, {useState, useEffect} from 'react';
import { 
    View, 
    SafeAreaView,
    SectionList,
    Text,
  } from 'react-native';


const FoodItemsScreen = ( {navigation, route} ) => {

    const {category} = route.params;
    const [items, setItems] = useState ([]);

    const getFoodItems = async () => {
        if (category == "Food") {
            // call api
            const response = await fetch("http://localhost:3000/restaurants");
            const foodItems = await response.json();
            setItems(foodItems);
        }
    }
    
    useEffect( () => {
        getFoodItems()
    }, []);

    return (
        <SafeAreaView>
            <View style={{
                paddingTop: 20}}>
                    <SectionList 
                    sections={items}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({item}) => (
                        <View style={{
                            backgroundColor: "#D3FFF6", 
                            padding: 10, marginVertical: 2, 
                            flexDirection: "row", 
                            justifyContent: "space-between"}}>
                            <Text style={{fontSize: 18}}>{item.menu}</Text>
                            <Text style={{fontSize: 18}}>{item.price} nok</Text>
                        </View>
                        )}
                    renderSectionHeader={({ section: { name, address } }) => (
                        <View>
                            <Text style={{fontSize: 24, backgroundColor: "#fff"}}>{name}</Text>
                            <Text style={{fontSize: 18, backgroundColor: "#fff"}}>{address}</Text>
                        </View>
                    )}
                    />
            </View>
        </SafeAreaView>
    );
}

export default FoodItemsScreen;

```

### Implementing 'MusicItems' screen
Same techniques as above can be used to display 'Music' items i.e songs when user presses 'Music' card in the home screen.

Let´s add some dummy data (we will add data via api later on). Sample contents for 'dummydata.json' is like below. It contains couple of restaurants and one song!

```
{
  "restaurants": [
    {
      "id": 1,
      "name": "Chinese Restaurent",
      "address": "Prinsengata 1, 7010 Trondheim",
      "data": [
        {
          "menu": "Pizza",
          "price": 50
        },
        {
          "menu": "Burger",
          "price": 25
        },
        {
          "menu": "Risotto",
          "price": 250
        }
      ]
    },
    {
      "id": 2,
      "name": "Mexican Restaurent",
      "address": "Kongensgata 10, 7020 Trondheim",
      "data": [
        {
          "menu": "French Fries",
          "price": 250
        },
        {
          "menu": "Onion Rings",
          "price": 150
        },
        {
          "menu": "Fried Shrimps",
          "price": 20
        }
      ]
    },
    {
      "id": 3,
      "name": "Indian Restaurent",
      "address": "Munkegata 15, 7030 Trondheim",
      "data": [
        {
          "menu": "Water",
          "price": 15
        },
        {
          "menu": "Coke",
          "price": 150
        },
        {
          "menu": "Beer",
          "price": 50
        }
      ]
    },
    {
      "id": 4,
      "name": "New Chinese Restaurent 1",
      "address": "Prinsengata 21, 7010 Trondheim",
      "data": [
        {
          "menu": "Pizza",
          "price": 50
        },
        {
          "menu": "Burger",
          "price": 25
        },
        {
          "menu": "Risotto",
          "price": 250
        }
      ]
    }
  ],
  "vechiles": [],
  "mobileDevices": [],
  "realstateProperties": [],
  "songs": [
    {
      "id": 57945,
      "title": "song 57945 title",
      "artist": "artist 57945",
      "link": "youtube.com/57945"
    }
  ],
  "profilesForDate": []
}
```

Now start the 'json-server' if not started yet! 

Now its again time to write a 'Screen' component and register it to the react-navitation container in the app. This is done in the following steps.

- Implement a 'screen' as react component as below. Note that: songs are retrieved using an API call, 'MucicItem' reusable UI component is defined within the same file and it taks 'song' as property which is used to render UI elements, main render funtion is implemented similar as in 'FoodItems' screen.

    <img src="song_item.png" align="right" width=200>

    ```
    // contents of MusicItems.js
    import React, {useState, useEffect} from 'react';
    import { 
        View, 
        SafeAreaView,
        Text
    } from 'react-native';

    const MusicItemsScreen = ( {navigation, route} ) => {

        const [items, setItems] = useState ([]);

        const getSongs = async () => {
            // call api
            const response = await fetch("http://localhost:3000/songs");
            const songs = await response.json();
            setItems(songs);
        };

        useEffect ( () => {getSongs()}, []);

        const MucicItem = ({song}) => {
            return (
                <View style={{ 
                    margin: 5,
                    backgroundColor: "#FFDFFD",
                    }}>
                    <Text> Title: {song.title} </Text>
                    <Text> Artist: {song.artist} </Text>
                    <Text> Link: {song.link} </Text>
                </View>
            );
        };

        return (
        <SafeAreaView> 
            {
                items.map((value, index) => {
                        return <MucicItem key={value.id} song={value}/>
                    }
                )
            }
            
        </SafeAreaView>
        );
    }

    export default MusicItemsScreen;
    ```
    <br clear="both">
- Register it in 'App.js' file. This is done by adding one more 'Stack.Screen' component representing 'MusicItemsScreen' in the 'App.js' file. Like below. By doing this, 'MusicItemsScreen' is now available for navigation, for example from other screens.

    ```
    import React, {useState} from 'react';

    import { NavigationContainer } from '@react-navigation/native';
    import { createNativeStackNavigator } from '@react-navigation/native-stack';

    import HomeScreen from './screens/Home';
    import FoodItemsScreen from './screens/FoodItems';
    import MusicItemsScreen from './screens/MusicItems';

    const Stack = createNativeStackNavigator();

    const Inft2508App = () => {
    return (
        <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen 
            name="Home"
            component={HomeScreen}
            />
            <Stack.Screen
            name="FoodItems"
            component={FoodItemsScreen}
            />
            <Stack.Screen
            name="MusicItems"
            component={MusicItemsScreen}
            />
        </Stack.Navigator>
        </NavigationContainer>
    );
    }

    export default Inft2508App;
    ```
- Update 'Home' screen i.e call the new 'MusicItemsScreen' upon click of 'Music' card. This can be done by this statement ```navigation.navigate("MusicItems")```. Complete code for 'Home.js' file is following. Specifically note the 'showCardItems' method where nagivation to 'MusicItemsScreen' is located!
    ```
    import React, {useState} from 'react';
    import { 
        View, 
        TextInput, 
        ScrollView, 
        SafeAreaView
    } from 'react-native';
    import Card from '../components/Card'

    const HomeScreen = ( {navigation} ) => {
        const logos = {
            logo_music: require('../images/logo-music.png'),
            logo_vechiles: require('../images/logo-vechiles.png'),
            logo_food: require('../images/logo-food.png'),
            logo_realstate: require('../images/logo-realstate.jpeg'),
            logo_dating: require('../images/logo-dating.png'),
            logo_mobile: require('../images/logo-mobile.png'),
        };
    
        // initial state
        const initialState = [
            {id: 1, displayText: "Music", logo: logos.logo_music},
            {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
            {id: 3, displayText: "Food", logo: logos.logo_food},
            {id: 4, displayText: "Real State", logo: logos.logo_realstate},
            {id: 5, displayText: "Dating", logo: logos.logo_dating},
            {id: 6, displayText: "Mobile", logo: logos.logo_mobile},
            {id: 7, displayText: "Music", logo: logos.logo_music},
            {id: 8, displayText: "Vechiles", logo: logos.logo_vechiles},
            {id: 9, displayText: "Food", logo: logos.logo_food},
            {id: 10, displayText: "Real State", logo: logos.logo_realstate},
            {id: 11, displayText: "Dating", logo: logos.logo_dating},
            {id: 12, displayText: "Mobile", logo: logos.logo_mobile},
            {id: 13, displayText: "Music", logo: logos.logo_music},
            {id: 14, displayText: "Vechiles", logo: logos.logo_vechiles},
            {id: 15, displayText: "Food", logo: logos.logo_food},
            {id: 16, displayText: "Real State", logo: logos.logo_realstate},
            {id: 17, displayText: "Dating", logo: logos.logo_dating},
            {id: 18, displayText: "Mobile", logo: logos.logo_mobile},
        ]
    
        // set initial states
        const [cards, setCards] = useState (initialState);

        const showCardItems =  (category) => {
            // navigate to items page
            if (category == "Food") {
                navigation.navigate("FoodItems", {category: category}) 
            } else if (category == "Music") {
                navigation.navigate("MusicItems")
            }
        }

        return (
            <SafeAreaView>
                <View style= {{ margin: 10, alignItems: "center" }}>
                    <TextInput
                    placeholder="Type here to search!"
                    onChangeText={newText => {
                        var matchedCards = [];
                        // apply search only if search string length >= 2
                        if(newText.trim().length >= 2) {
                        initialState.map((value, index) => {
                            if(value.displayText.includes(newText)) {
                            matchedCards.push(value);
                            }
                        });
                        setCards(matchedCards);
                        } else{
                        setCards(initialState);
                        }
                    }}
                    />
                </View>
                    <ScrollView>
                    <View style={{ 
                        flex: 1, 
                        justifyContent: "center", 
                        alignItems: "center",
                        flexDirection: "row",
                        flexWrap: "wrap",
                        alignContent: "center",
                        }}>

                        {
                            cards.map((value, index) => {
                                return <Card key={value.id} displayText={value.displayText} logo={value.logo} showItems={showCardItems}/>
                            }
                            )
                        }
                    </View>
                    </ScrollView>
                </SafeAreaView>
        );
    }

    export default HomeScreen;
    ```

Now that we have a new screen for showing music items and the items themselves are retrieved from server using REST API calls.

In the next section, we will focus on 'Adding data' to the server from mobile app.

### Adding data via REST API
Let´s assume a scenario where mobile app should be able to add data records to the server. For the sake of demo/testing, the scenario is considered as following!

 >Scenario: Let´s assume that our app allows users to add songs they like. It’s a kind of bookmarking or sharing​. For example, add plus sign like icon towards the top of the MusicItemsScreen component. Upon press of that icon, do a REST API call (http POST) to the server with a data that corresponds a song - for example song title, song artist and a link to the song for example youtube link!


For this scenario to realize, let´s update our 'MusicItems.js' file.

First we will add a 'MusicHeader' UI part that includes 'Image' component wrapped with 'Pressable' component. Upon press, we will call a new (to be implemented) function called 'addSong'. Resulting updated 'MusicItems.js' file will look like following.


<img align="right" src="add_song.png" width=250>

```
// updated contents of MusicItems.js file

import React, {useState, useEffect} from 'react';
import { 
    View, 
    SafeAreaView,
    Text,
    Image,
    Pressable
  } from 'react-native';

const MusicItemsScreen = ( {navigation, route} ) => {

    const [items, setItems] = useState ([]);

    const getSongs = async () => {
        // call api
        const response = await fetch("http://localhost:3000/songs");
        const songs = await response.json();
        setItems(songs);
    };

    const addSong = async () => {

        // random number between 0 and 1000
        const randomNumber = Math.floor(Math.random() * 100000);

        // call api
        const response = await fetch("http://localhost:3000/songs",
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify ({
                    id: randomNumber,
                    title: 'song ' + randomNumber + ' title',
                    artist: 'artist ' + randomNumber,
                    link: 'youtube.com/' + randomNumber
                })
            });
        const songAdded = await response.json();
        console.log(songAdded);
    };

    useEffect ( () => {getSongs()}, []);

    const MucicItem = ({song}) => {
        return (
            <View style={{ 
                margin: 5,
                backgroundColor: "#FFDFFD",
                }}>
                <Text> Title: {song.title} </Text>
                <Text> Artist: {song.artist} </Text>
                <Text> Link: {song.link} </Text>
            </View>
        );
    };
    const MusicHeader = () => {
        return (
            <View style={{ 
                padding: 5,
                alignItems: "center",
                justifyContent: "center",
                }}>
                <Pressable onPress ={addSong}>
                    <Image 
                        style= {{width: 40, height: 40}} 
                        source={require('../images/plus-sign.png')}
                    />
                </Pressable>
            </View>
        );
    };

    return (
    <SafeAreaView> 
        <MusicHeader/>
        {
            items.map((value, index) => {
                    return <MucicItem key={value.id} song={value}/>
                }
            )
        }
        
    </SafeAreaView>
    );
}

export default MusicItemsScreen;
```

<br clear="both">

Note: If you inspect our new 'addSong' method, you will realize that song data is semi randomly generated! In a real world scenario where will a UI (screen or dialog or similar) to collect or enter song related data. For simplicity we have skipped to UI for this part. Readers are encouraged to implement UI for this part!

Another issue with the above implementation is that eventhough 'song' related data is saved/updated in the server, UI is not immediately refreshed! Let´s solve this issue in the next section!

### Refreshing UI with updated data
Usually UI in react-native app rerendered whenever a state of a component changes - for example one or more state variables change. Keeping this in mind there are multiple ways of refreshing or rerendering UI when the data is updated for example when a new song is added from the app. Two possibilities are menitioned below

- When new song is added from the app via REST API call, check the response. If response has success code, append the locally availble and recently added object (ie Song) to the list of existing songs items in the MusicItems component state.
- When new song is added from the app via REST API call, check the response. If response has success code, make another REST API call to get the latest data, and replace the existing music state items with the new data from API call. In the following we will implement this approach!

Since we already have a method 'getSongs' to get lists of songs via REST API, and we want to reuse that! Note that the method is already in 'useEffect' hook. Sor far, the useEffect hook calls the getSongs method once at the initial rendering. Since the hook provides possibilities to call the callback function based on dependencies passed as arguments, we will use this technique to render UI witl the added/updated data as following

- Add a state variable, let´s name is 'refresh' and set default/initial value as 'false'. Like this, ``` const [refresh, setRefresh] = useState(false);```
- Then, update the 'refresh' state variable at the end of the 'addSong' method. Like this, ```setRefresh(true);```
- Then, we want to call 'getSongs' function when state variable 'refresh' is called. This can be done by passing 'refresh' state variable as dependency in 'useEffect' hook

If we do these changes and run the app we will notice that the UI will rendered with the newly added song only one - the very first added item! Why?

The reason for that is following. When 'addSong' method is called we set the value of the 'refresh' to 'true'. That means 'useEffect' detects the changes and callback function is called i.e getSong methos is called which eventually shanges songs via 'setItems' method and UI is rendered with newly added item (the first added item).

When the second item is added via REST API, 'addSong' method is called and that set the value of the 'refresh' to 'true'. Since the existing value of 'refresh' is already true, not change in value of 'refresh' will be detected and therefore 'useEffect' hook will not run its callback function. That means 'getItems' method will not be called that means newly added song item will not be retrieved from the server. That also means UI will not display the newly added item!!

The above mentioned problem can be solved if we can somehow reset the value of the 'refresh' state variable in between the 'useEffect' callback function calls. This can actually be achieved by using so called 'cleanup' function with the 'useEffect' hook. And with the cleanup function we can reset the value of 'refresh' to 'false' again. By doing so, cleanup function is invoked before the callback function is invoked, that means changes in 'refresh' variable is detected and 'getSongs' function is called! 

The updated code for 'MusicItems.js' is below. If you run the app and inspect, the behavior is correct now. Whenver a 'plus sign' is clicked on the UI, new item is added to the server and UI is also updated correctly.

<img src="ui_refresh.png" align="right" width=300>

```
// contents of MusicItems.js

import React, {useState, useEffect} from 'react';
import { 
    View, 
    SafeAreaView,
    Text,
    Image,
    Pressable
  } from 'react-native';

const MusicItemsScreen = ( {navigation, route} ) => {

    const [items, setItems] = useState ([]);
    const [refresh, setRefresh] = useState(false);

    const getSongs = async () => {
        // call api
        const response = await fetch("http://localhost:3000/songs");
        const songs = await response.json();
        setItems(songs);
    };

    const addSong = async () => {

        // random number between 0 and 1000
        const randomNumber = Math.floor(Math.random() * 100000);

        // call api
        const response = await fetch("http://localhost:3000/songs",
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify ({
                    id: randomNumber,
                    title: 'song ' + randomNumber + ' title',
                    artist: 'artist ' + randomNumber,
                    link: 'youtube.com/' + randomNumber
                })
            });
        const songAdded = await response.json();
        setRefresh(true);
    };

    useEffect ( () => {
        getSongs();
        return function cleanup(){
            setRefresh(false);
        }
    }, [refresh]);

    const MucicItem = ({song}) => {
        return (
            <View style={{ 
                margin: 5,
                backgroundColor: "#FFDFFD",
                }}>
                <Text> Title: {song.title} </Text>
                <Text> Artist: {song.artist} </Text>
                <Text> Link: {song.link} </Text>
            </View>
        );
    };
    const MusicHeader = () => {
        return (
            <View style={{ 
                padding: 5,
                alignItems: "center",
                justifyContent: "center",
                }}>
                <Pressable onPress ={addSong}>
                    <Image 
                        style= {{width: 40, height: 40}} 
                        source={require('../images/plus-sign.png')}
                    />
                </Pressable>
            </View>
        );
    };

    return (
    <SafeAreaView> 
        <MusicHeader/>
        {
            items.map((value, index) => {
                    return <MucicItem key={value.id} song={value}/>
                }
            )
        }
        
    </SafeAreaView>
    );
}

export default MusicItemsScreen;

```

<br clear="both">



## Exercise #6
This exercise will also be a follow-up exercise from previous exercises (upto exercise#5 https://gitlab.com/sysdev-tutorials/react-native/06-multi-page-or-screen-application#exercise-5). 
The key tasks for this exercise are following

 - Instead of dummy data, use json-server as your REST API providor. If you wish, you are also encouraged to implement you real API for example using spring-boot ot python or similar!
 - In your mobile app, use 'fetch' to retrieve and update data - including GET, POST, PUT and DELETE http request types

